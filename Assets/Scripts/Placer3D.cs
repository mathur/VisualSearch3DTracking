﻿//I place 3D objects!! :)

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class Placer3D : MonoBehaviour {
    public static int blockNum = 0;

    public static short state; //state of the placer


    public Material[] distractorTargetMaterials; //Materials for distractor and target
    public GameObject cube; //Assigned in editor
    public GameObject sphere; //Assigned in editor

    
    private GameObject[] objectArray;
   
    private static int numTrials;
    private ExperimentWrapper OurExperiment;

    private int trialNumCounter = 0; //Counts the trialNum as participants progress
    private int randomIndex=0;

	// Use this for initialization
	void Start () {

        Debug.Log("We start reading at " + System.DateTime.Now);

        StreamReader reader;

        //Give a value to firstBlock depending on the argument passed 
        if(System.Environment.GetCommandLineArgs().Length == 3)
        {
            string arg = System.Environment.GetCommandLineArgs()[2];
            blockNum = int.Parse(arg); //Parse the block number into an integer
        }
        else
        {
            Debug.LogError("Incorrect block number specified... Defaulting to 1.");
            blockNum = 1;
        }

        if (blockNum == 1)
            reader = new StreamReader("config-mod-1.txt");
        else if (blockNum == 2)
            reader = new StreamReader("config-mod-2.txt");
        else
            reader = new StreamReader("config-mod-3.txt");

        state = 0; //init state

        //Our Experiment
        OurExperiment = new ExperimentWrapper(); //Init the list

        //Read the first JSON object
        string readData = reader.ReadLine(); //1

        
        //Then, read in a loop
        
        while (readData != null) //1
        {
            OurExperiment.Trials.Add(JsonUtility.FromJson<SceneDescription>(readData));
            OurExperiment.numOfStimuli++;
            readData = reader.ReadLine();
        }
     

        Debug.Log("Number of trials retrieved are " + OurExperiment.numOfStimuli.ToString());

        Debug.Log("We end reading at " + System.DateTime.Now);

    }
	
	// Update is called once per frame
	void Update () {
        //state == 0 means waiting on fixation

        if(state == 1) //Time to display stuff
        {
            
            randomIndex = Random.Range(0, OurExperiment.Trials.Count); //randomization

            trialNumCounter++; //A new trial has begun

            Debug.Log("Started trial number " + trialNumCounter.ToString() + " with " + OurExperiment.Trials[randomIndex].objects.Count + " distractors");


            ControllerMaster.trialNum = trialNumCounter;
            ControllerMaster.currScene = OurExperiment.Trials[randomIndex]; //Set relevant variable used by the master


            objectArray = new GameObject[OurExperiment.Trials[randomIndex].numOfObjects]; //Array init with size

            //Now, let us start the placement
            for (int i=0; i < OurExperiment.Trials[randomIndex].numOfObjects; i++)
            {
                //create object
                if(OurExperiment.Trials[randomIndex].objects[i].shape)
                    objectArray[i] = Instantiate(cube, OurExperiment.Trials[randomIndex].objects[i].position  + transform.position, /*OurExperiment.Trials[randomIndex].objects[i].rotation*/ Quaternion.identity) as GameObject;
                else
                    objectArray[i] = Instantiate(sphere, OurExperiment.Trials[randomIndex].objects[i].position + transform.position, /*OurExperiment.Trials[randomIndex].objects[i].rotation*/ Quaternion.identity) as GameObject;

                //set material
                objectArray[i].GetComponent<Renderer>().material = distractorTargetMaterials[OurExperiment.Trials[randomIndex].objects[i].material];
            }


            Debug.Log("Target 1 is in Region " + OurExperiment.Trials[randomIndex].regionID_1.ToString() + " coloured " + OurExperiment.Trials[randomIndex].region1Colour);
            Debug.Log("Target 2 is in Region " + OurExperiment.Trials[randomIndex].regionID_2.ToString() + " coloured " + OurExperiment.Trials[randomIndex].region2Colour);

            state = 2; //Once, everything is done, set state to two -> displayed everything, waiting for user decision

            OurExperiment.Trials.RemoveAt(randomIndex); //Remove this trial as it has been placed
        }


        if (state == -1) //clean-up time
        {

            if (objectArray.Length > 0)
            {
                foreach (GameObject go in objectArray)
                    DestroyObject(go);
            }

            state = 0; //Reset placer state

            if (OurExperiment.Trials.Count == 0) //No trial left in the list
            {
                ControllerMaster.testOver = true;
                state = 99;
            }
            //randomIndex++;//sequential

        }

	}
}
