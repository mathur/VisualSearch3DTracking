﻿//Data structure for serializing pose information

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PoseInfo
{
    public string subjectID;
    public int stimulusNum;

    public Vector3 headPos, torsoPos;

    public Vector3 headRotEuler, torsoRotEuler;
    public Quaternion headRot, torsoRot;

    public int trialNum;
    public string timeStamp;

    public PoseInfo(string sId, int sNum, int tNum, Vector3 hP, Vector3 tP, Quaternion hR, Quaternion tR)
    {
        subjectID = sId;
        stimulusNum = sNum;
        trialNum = tNum;
        headPos = hP;
        torsoPos = tP;
        headRot = hR;
        torsoRot = tR;
        headRotEuler = headRot.eulerAngles;
        torsoRotEuler = torsoRot.eulerAngles;
        timeStamp = System.DateTime.Now.ToString("HH-mm-ss.fff");
    }

}
