﻿//Generates trials for the experiment

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;

[System.Serializable]
public class ObjectDescription
{
    public Vector3 position; //position of object
    //public Quaternion rotation = Quaternion.identity; //rotation of object
    public bool shape; //true for cube, false for sphere
    public short material; //colour 0- green 1-red 2-yellow 3-blue
    public Vector3 sphericalCoord;
    public ObjectDescription(Vector3 a, /*Quaternion b,*/ bool c, short d, Vector3 e)
    {
        position = a;
        //rotation = b;
        shape = c;
        material = d;
        sphericalCoord = e;
    }
    public ObjectDescription (ObjectDescription copy)
    {
        position = copy.position;
        //rotation = b;
        shape = copy.shape;
        material = copy.material;
        sphericalCoord = copy.sphericalCoord;
    }
}

[System.Serializable]
public class SceneDescription //wrapper for the scene
{
    public int stimulusNumber;
    public int numOfObjects; //num of objects in scene


    public int regionID_1 = -1; //RegionID of the first object (target if it exists)
    public int regionID_2 = -1; //Two region ids specifically for the opposing region experiment

    public bool twoTargets; //Are there two targets
    
    public string region1Colour;
    public string region2Colour;

    public bool targetExists; //whether there is a target
    public bool isFeature; //true- feature search, false- conjunctive search

    public List<ObjectDescription> objects; //list of all objects

    public SceneDescription(int counter, int a, bool b, bool c, int rid1, int rid2)
    {
        stimulusNumber = counter;
        numOfObjects = a;
        targetExists = b;
        isFeature = c;
        regionID_1 = rid1; //Set regionId
        regionID_2 = rid2;
    }
}


public class ExperimentWrapper //The wrapper for all the scenes (simple reads not supported)
{
    public int numOfStimuli;
    public List<SceneDescription> Trials;
    public ExperimentWrapper()
    {
        numOfStimuli = 0; //Only used for easy readability
        Trials = new List<SceneDescription>();
    }
}


public class RandomizedSphereGenerator : MonoBehaviour
{
    public float maxDistance = 7f;
    public float probTarget = 1.0f;
    public float minDistanceBetweenObjects = 0.22f; //This is used to avoid collisions and occlusions
    private int numOfObjects; //num of objects in scene
    private int numOfStimulus = 0; //start with a 1 from Trial Line Number
    private float epsilon = 0.04f; //Maintain space between various regions

    //Do this twice for the negative range


    private static float[] polarRanges = new float[] { -Mathf.PI / 2f, -Mathf.PI / 4f, 0, Mathf.PI / 4f, Mathf.PI / 2f };
    private static float[] elevationRanges = new float[] { 0f, Mathf.PI / 4f, Mathf.PI / 2f, Mathf.PI * 3f / 4f, Mathf.PI, Mathf.PI * 5f / 4f, Mathf.PI * 3f / 2f, Mathf.PI * 7f / 4f, Mathf.PI * 2f };
    
    //private static float[] polarRanges = new float[9] { 0f, Mathf.PI / 4f, Mathf.PI / 2f, Mathf.PI * 3f / 4f, Mathf.PI, Mathf.PI * 5f / 4f, Mathf.PI * 3f / 2f, Mathf.PI * 7f / 4f, Mathf.PI * 2f };
    //private static float[] elevationRanges = new float[5] { 0f, Mathf.PI / 4, Mathf.PI / 2, Mathf.PI * 3f / 4f, Mathf.PI };
    //private static float[] polarRanges = new float [3] { Mathf.PI * 3f / 4f, Mathf.PI, Mathf.PI * 5f / 4f  };
    //private static float[] elevationRanges = new float[3] { Mathf.PI / 4, Mathf.PI / 2, Mathf.PI * 3f / 4f };

    private static int[] progressionOfDistractors = new int[] { 96, 480, 768, 1024 };// new int[4] { 96, 480, 768, 1024 };//new int[1] { 96 };//new int[3] { 96, 480, 1024};//
 

    private int[,] competingRegions = new int[,] { { 5, 8 }, { 25, 28 }, { 9, 12 }, { 21, 24 }, { 8, 25 }, { 5, 28 }, { 12, 21 }, { 9, 24 } };//{ { 5, 8 }, { 25, 28}, { 9, 12 }, { 21, 24 }, { 8, 25 }, { 5, 28 }, { 12, 21 }, {9,24} };
    private string[] colourSchemes = new string[6] { "RB", "GR", "GB", "BR", "RG", "BG"}; //The different colour schemes for each competing region configuration

    void Awake()
    {

        Debug.Log("We started generation at " + System.DateTime.Now);

        StreamWriter writer = new StreamWriter("config-mod.txt"); //Exhaustive version
        for (int iter = 0; iter < 4; iter++)
        {
            for (int idDistractor = 0; idDistractor < progressionOfDistractors.Length; idDistractor++)
            {//For all densities of distractors
                numOfObjects = progressionOfDistractors[idDistractor]; //Number of objects in the scene
                for (int idCompetingRegions = 0; idCompetingRegions < competingRegions.GetLength(0); idCompetingRegions++)
                {//foreach competing region
                    int reg1 = competingRegions[idCompetingRegions, 0];
                    int reg2 = competingRegions[idCompetingRegions, 1];

                    //Initialize the scene with 0 objects
                    SceneDescription currentScene = new SceneDescription(numOfStimulus, 0, true, true, reg1, reg2);
                    currentScene.objects = new List<ObjectDescription>();

                    List<ObjectDescription> targetRegObjects = new List<ObjectDescription>(); //Temporary list that helps store all objects in target region1

                    int indexObjReg1 = -1, indexObjReg2 = -1; //Stores the index of the object at the region of interest

                    int regionCounter = 0; //Counts regions as objects are being filled in 

                    for (int verCounter = 0; verCounter < (elevationRanges.Length - 1); verCounter++) //For all vertical regions
                    {//Place objects
                        for (int horCounter = 0; horCounter < (polarRanges.Length - 1); horCounter++) //For all horizontal regions
                        {
                        
                            //Declare some vars
                            Vector3 chosenPolar; //This is used as a temporary variable
                            Vector3 position; //holds the temp position
                            int tempCounter = 0;
                            regionCounter++; //Increment region number
                                             //Keeps track of what has been included and what not
                            
                            do
                            {
                                ObjectDescription tempObject; //A temporary object that may be used for targets or later for distractors
                                bool cannotUseRand = false; //reset

                                if (elevationRanges[verCounter] < Mathf.PI / 2f  || elevationRanges[verCounter] > Mathf.PI * 3f / 2f)
                                {
                                    chosenPolar = new Vector3(Random.Range(2f, maxDistance), Random.Range(polarRanges[horCounter] + epsilon, polarRanges[horCounter + 1] - epsilon),
                                        Random.Range(elevationRanges[verCounter] + epsilon, elevationRanges[verCounter + 1] - epsilon));
                                }

                                else
                                {
                                    chosenPolar = new Vector3(Random.Range(2f, maxDistance), Random.Range(-polarRanges[horCounter] - epsilon, -polarRanges[horCounter + 1] + epsilon),
                                        Random.Range(elevationRanges[verCounter] + epsilon, elevationRanges[verCounter + 1] - epsilon));

                                }

                                    SphericalToCartesian(chosenPolar.x, chosenPolar.y, chosenPolar.z, out position);


                                //Check for possible collision
                                for (int id = 0; id < currentScene.numOfObjects; id++)
                                {
                                    if ((Mathf.Abs(currentScene.objects[id].position.x - position.x) < minDistanceBetweenObjects) && (Mathf.Abs(currentScene.objects[id].position.y - position.y) < minDistanceBetweenObjects)
                                        && (Mathf.Abs(currentScene.objects[id].position.z - position.z) < minDistanceBetweenObjects))
                                    {
                                        cannotUseRand = true;
                                        break; //break out of the loop
                                    }
                                }

                                if (tempCounter + 1 == numOfObjects / ((polarRanges.Length - 1) * (elevationRanges.Length - 1))) //If target object is being created
                                {
                
                                    Vector3 position2 = Vector3.Reflect(position, Vector3.forward);
                                    if (Mathf.Abs((position2.z) - (position.z)) < 5.2f) //Maintain a minimum distance to mirror
                                    {
                                        cannotUseRand = true;
                                    }

                                }

                                if (cannotUseRand)
                                    continue;

                                else
                                {
                                    if(tempCounter < numOfObjects /(2 * ((polarRanges.Length - 1) * (elevationRanges.Length - 1))))
                                        tempObject = new ObjectDescription(position, false, 0, chosenPolar); //sphere
                                    else
                                        tempObject = new ObjectDescription(position, true, 0, chosenPolar); //cube

                                    //Temporary demo of target

                                    /*
                                     GameObject go = Instantiate(GameObject.CreatePrimitive(PrimitiveType.Cube), position, Quaternion.identity) as GameObject;
                                     go.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                                     */

                                    //Temporary demo of target

                                    /*
                                    if (regionCounter == 9)
                                        tempObject.material = 2;
                                    if (regionCounter == 6)
                                        tempObject.material = 1;
                                    if (regionCounter == 24)
                                        tempObject.material = 3;

                                    */

                                    if (regionCounter == reg1) //If this region is reg1
                                    {
                                        indexObjReg1 = currentScene.objects.Count; //Add this object as target

                                        ObjectDescription od = new ObjectDescription(tempObject);
                                        targetRegObjects.Add(od);
                                    }
                                    if (regionCounter == reg2)
                                    {
                                        break;
                                    }

                                    currentScene.objects.Add(tempObject); //Add to the list
                                    currentScene.numOfObjects++; //increment the number of objects
                                    tempCounter++; //increment helper variable for the loop

                                }
                            } while (tempCounter < numOfObjects / ((polarRanges.Length - 1) * (elevationRanges.Length - 1))); //Until the tempCounter is smaller
                                                                                                                              //Write modified objects for each colour scheme

                        }
                    } //All objects have been placed

                    if(idCompetingRegions < 4) //Opposing region along same axis
                    {
                        foreach (ObjectDescription obj in targetRegObjects)
                        {
                            Debug.Log("Before : " + obj.position.ToString()); 
                            obj.position = Vector3.Reflect(obj.position, Vector3.forward);
                            Debug.Log("After : " + obj.position.ToString());
                            obj.sphericalCoord = new Vector3(-1f, -1f, -1f);
                            indexObjReg2 = currentScene.objects.Count;
                            currentScene.objects.Add(obj);
                            currentScene.numOfObjects++;
                        }
                    }
                    else //Opposing region located diagonally
                    {
                        foreach (ObjectDescription obj in targetRegObjects)
                        {
                            obj.position = Vector3.Reflect(obj.position, Vector3.forward);
                            obj.position = Vector3.Reflect(obj.position, Vector3.down);
                            obj.sphericalCoord = new Vector3(-1f, -1f, -1f);
                            indexObjReg2 = currentScene.objects.Count;
                            currentScene.objects.Add(obj);
                            currentScene.numOfObjects++;
                        }
                    }

                    //Now, we make 6 case distinctions based on colourSchemes

                    //"RB",
                    numOfStimulus++; //Increment stimulus number
                    currentScene.stimulusNumber++; //Also increment inside the data structure

                    currentScene.objects[indexObjReg1].material = 1;
                    currentScene.objects[indexObjReg2].material = 3;
                    currentScene.twoTargets = true;
                    currentScene.region1Colour = "Red";
                    currentScene.region2Colour = "Blue";
                    writer.WriteLine(JsonUtility.ToJson(currentScene, false));

                    //"GR", 
                    numOfStimulus++; //Increment stimulus number
                    currentScene.stimulusNumber++; //Also increment inside the data structure

                    currentScene.objects[indexObjReg1].material = 0;
                    currentScene.objects[indexObjReg2].material = 1;
                    currentScene.twoTargets = false;
                    currentScene.region1Colour = "Green";
                    currentScene.region2Colour = "Red";
                    writer.WriteLine(JsonUtility.ToJson(currentScene, false));

                    //"GB", 
                    numOfStimulus++; //Increment stimulus number
                    currentScene.stimulusNumber++; //Also increment inside the data structure

                    currentScene.objects[indexObjReg1].material = 0;
                    currentScene.objects[indexObjReg2].material = 3;
                    currentScene.twoTargets = false;
                    currentScene.region1Colour = "Green";
                    currentScene.region2Colour = "Blue";
                    writer.WriteLine(JsonUtility.ToJson(currentScene, false));

                    //"BR", 
                    numOfStimulus++; //Increment stimulus number
                    currentScene.stimulusNumber++; //Also increment inside the data structure

                    currentScene.objects[indexObjReg1].material = 3;
                    currentScene.objects[indexObjReg2].material = 1;
                    currentScene.twoTargets = true;
                    currentScene.region1Colour = "Blue";
                    currentScene.region2Colour = "Red";
                    writer.WriteLine(JsonUtility.ToJson(currentScene, false));

                    //"RG", 
                    numOfStimulus++; //Increment stimulus number
                    currentScene.stimulusNumber++; //Also increment inside the data structure

                    currentScene.objects[indexObjReg1].material = 1;
                    currentScene.objects[indexObjReg2].material = 0;
                    currentScene.twoTargets = false;
                    currentScene.region1Colour = "Red";
                    currentScene.region2Colour = "Green";
                    writer.WriteLine(JsonUtility.ToJson(currentScene, false));

                    //"BG"
                    numOfStimulus++; //Increment stimulus number
                    currentScene.stimulusNumber++; //Also increment inside the data structure

                    currentScene.objects[indexObjReg1].material = 3;
                    currentScene.objects[indexObjReg2].material = 0;
                    currentScene.twoTargets = false;
                    currentScene.region1Colour = "Blue";
                    currentScene.region2Colour = "Green";
                    writer.WriteLine(JsonUtility.ToJson(currentScene, false));

                }
            }
        }

        writer.Close(); //close the file
   
        Debug.Log("We end generation at " + System.DateTime.Now);
        Debug.Log("We wrote " + numOfStimulus.ToString() + " number of trials for the experiment");
    }


    public static void SphericalToCartesian(float radius, float azimuth, float inclination, out Vector3 cartesianCoord)
    {
        float a = radius * Mathf.Cos(inclination);
        cartesianCoord = new Vector3(a * Mathf.Cos(azimuth), radius * Mathf.Sin(inclination), a * Mathf.Sin(azimuth));
    }

    //Rotation of objects along axes
    public Vector3 RotY(Vector3 origPos)
    {
        Quaternion rotation = Quaternion.Euler(0f, 135f, 0f);
        return rotation * origPos;
    }

    public Vector3 RotZ(Vector3 origPos)
    {
        Quaternion rotation = Quaternion.Euler(0f, 0f, 180f);
        return rotation * origPos;
    }

    public Vector3 RotX(Vector3 origPos)
    {
        Quaternion rotation = Quaternion.Euler(180f, 0f, 0f);
        return rotation * origPos;
    }
}

 