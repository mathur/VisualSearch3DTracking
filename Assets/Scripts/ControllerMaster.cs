﻿//Attached to the master gameobject
//Uses Placer3D.state and LookingAtFixationPlane.valid

using UnityEngine;
using System.Collections;
using System.IO;
using System.Collections.Generic;

public class ControllerMaster : MonoBehaviour
{
    public static int startScreenFrameNum = 0; //This is now being used in conjunction with amIEnabled to sift through info screens at the beginning of the experiment
    public static bool amIEnabled = false;

    public static bool testOver; //Is the test complete
    public static bool questionTarget = false;


    public static SceneDescription currScene = null; // Useful when writing to the result file
    public static int trialNum;
    public static int lastDoneAt=0; //This enables screens that force rest at appropriate intervals

    public float probOfQuestion; //Is the target above or below question

    private string resultFileName; //What's the filename?
    private StreamWriter writer; //Writer of result

    private StreamWriter poseWriter; //Writer of tracked pose result
    private bool poseWriterSwitch = false; //Switch to track written stuff
    private List<PoseInfo> poses;


    public static GameObject[] toBeDisabled;
    private int deviceIndexR;
    private int deviceIndexL;
    private float timeAtStart;

    private static string nameOfUser;
    public GameObject controllerWithUI; //Set in the inspector
    //Tracking vars
    public GameObject head, torso;
    private Vector3 headInitPos, torsoInitPos, headInitRot, torsoInitRot;
    
    // Use this for initialization
    void Start()
    {

        toBeDisabled = GameObject.FindGameObjectsWithTag("Fixation"); //make an array of stuff to quickly disable/enable

        //file creation
        nameOfUser = (System.Environment.GetCommandLineArgs()).Length > 1 ? (System.Environment.GetCommandLineArgs())[1] : "DummyUser";

        string  identifierForTest = nameOfUser; //use participant id as identifier

        resultFileName = identifierForTest + "_" + System.DateTime.Now.ToString("yyyy-dd-M--HH-mm") + ".csv";
        writer = new StreamWriter(resultFileName);
        writer.AutoFlush = true;
        poseWriter = new StreamWriter(identifierForTest + "_" + "TrackedPose"+ "_" + System.DateTime.Now.ToString("yyyy-dd-M--HH-mm") + ".txt");
        poseWriter.AutoFlush = true;
        
        //Add the header
        writer.WriteLine("Subject ID, Block Number, Trial Number, Type of Search, Stimulus Num, Num of Objects, Target# 1/2, Region#1, Region#2, Target Colour Region 1, Target Colour Region 2, Response Time, Selected Response, " +
            "Inferred Region, Inferred Target, Target Region, Correct / Incorrect");


        controllerWithUI.GetComponent<VRTK.VRTK_ControllerEvents>().TriggerClicked += new VRTK.ControllerInteractionEventHandler(Trigger); //Start listening for trigger click
    }

    // Update is called once per frame
    void Update()
    {
        deviceIndexR = SteamVR_Controller.GetDeviceIndex(SteamVR_Controller.DeviceRelation.Rightmost); // Done again and again to keep track of just woken up cotrollers
        deviceIndexL = SteamVR_Controller.GetDeviceIndex(SteamVR_Controller.DeviceRelation.Leftmost); // Done again and again to keep track of just woken up cotrollers

        if (trialNum == 192 && lastDoneAt < trialNum && amIEnabled == true)
        {                           
            amIEnabled = false; //Force this to false again
            startScreenFrameNum = 666; //Used to display rest time text
        }
        if (Placer3D.state == 0 && LookingAtFixationPlane.valid && !testOver) //user looking at fixation plane
        {
            //strict check for both controllers

            if ((deviceIndexR != -1 && deviceIndexL != -1) &&
                (SteamVR_Controller.Input(deviceIndexL).GetPressUp(SteamVR_Controller.ButtonMask.Grip) ||
                SteamVR_Controller.Input(deviceIndexR).GetPressUp(SteamVR_Controller.ButtonMask.Grip))) //Check for Grip press  
            {
                if (amIEnabled)
                {
                    timeAtStart = Time.time; //Set the time at start of trial

                    for (int i = 0; i < toBeDisabled.Length; i++)
                        toBeDisabled[i].SetActive(false);

                    trialNum = 0; //Reset trialNum

                    Placer3D.state = 1; //Start the placement 

                    poseWriterSwitch = true; //Set to true so that infinite loop in Coroutine would work
                    poses = new List<PoseInfo>(); //reset poses list
                    StartCoroutine(GetPoseEveryInstant());

                }
                else
                {
                    if (startScreenFrameNum <= 4)
                    {
                        startScreenFrameNum++; //Go to the next instruction screen
                    }
                    if(startScreenFrameNum == 5)
                        amIEnabled = true;
                    if (startScreenFrameNum == 666)
                    {
                        startScreenFrameNum = 5;
                        amIEnabled = true;
                        lastDoneAt = trialNum; //Set the lastDoneAt to trialNum so as to not be in an infinite loop
                    }
                }
            }
        }


        if(Input.GetKeyDown(KeyCode.Space))
        {
            Application.Quit();
        }
    }
    void OnApplicationQuit()
    {
        writer.Close();
        poseWriter.Close();
    }
    //"Subject ID, Block Number, Trial Number, Type of Search, Stimulus Num, Num of Objects, Target# 1/2, Region#1, Region#2, Target Colour Region 1, Target Colour Region 2, Response Time, Selected Response, " +
            //"Inferred Region, Inferred Target, Target Region, Correct / Incorrect"
    public void One()
    {
        if (questionTarget)
        {
            questionTarget = false; //Reset the variable

            string inferredRegion="Error", inferredTarget="Error", correctness = "Error";
            if (currScene.region1Colour == "Red")
                inferredRegion = currScene.regionID_1.ToString();
            else if (currScene.region2Colour == "Red")
                inferredRegion = currScene.regionID_2.ToString();
            else
                inferredRegion = "0";

            if (inferredRegion == "0")
            {
                inferredTarget = "0";
            }
            else
                inferredTarget = "Red";

            if (inferredRegion == "0") //Check correctness
            {
                correctness = "0";
            }
            else
                correctness = "1";

            //Target Region
            string targetRegion = "NA";
            if (currScene.region2Colour == "Green" && currScene.region1Colour != "Green")
            {
                targetRegion = currScene.regionID_1.ToString();
            }
            else if (currScene.region1Colour == "Green" && currScene.region2Colour != "Green")
            {
                targetRegion = currScene.regionID_2.ToString();
            }

            writer.WriteLine(nameOfUser + ", " + Placer3D.blockNum.ToString() + ", " + trialNum.ToString() + ", " + (currScene.isFeature? "Feature" : "Error") + ", " + currScene.stimulusNumber.ToString() + ", " + currScene.numOfObjects.ToString() + ", "
                + (currScene.twoTargets?"2":"1") + ", " + currScene.regionID_1.ToString() + ", " + currScene.regionID_2.ToString() + ", " + currScene.region1Colour + ", " + currScene.region2Colour + ", " 
                + (Time.time - timeAtStart).ToString() + ", " + "Red" + ", " + inferredRegion + ", " + inferredTarget + ", " + targetRegion + ", " + correctness);

            Debug.Log(nameOfUser + ", " + Placer3D.blockNum.ToString() + ", " + trialNum.ToString() + ", " + (currScene.isFeature ? "Feature" : "Error") + ", " + currScene.stimulusNumber.ToString() + ", " + currScene.numOfObjects.ToString() + ", "
                + (currScene.twoTargets ? "2" : "1") + ", " + currScene.regionID_1.ToString() + ", " + currScene.regionID_2.ToString() + ", " + currScene.region1Colour + ", " + currScene.region2Colour + ", "
                + (Time.time - timeAtStart).ToString() + ", " + "Red" + ", " + inferredRegion + ", " + inferredTarget + ", " + targetRegion + ", " + correctness);

            for (int i = 0; i < toBeDisabled.Length; i++) //Set active
                toBeDisabled[i].SetActive(true);

            currScene = null; //We do not need this anymore
        }
    }
    
    public void Two()
    {
        if (questionTarget)
        {
            questionTarget = false; //Reset the variable

            string inferredRegion = "Error", inferredTarget = "Error", correctness = "Error";
            if (currScene.region1Colour == "Blue")
                inferredRegion = currScene.regionID_1.ToString();
            else if (currScene.region2Colour == "Blue")
                inferredRegion = currScene.regionID_2.ToString();
            else
                inferredRegion = "0";

            if (inferredRegion == "0")
            {
                inferredTarget = "0";
            }
            else
                inferredTarget = "Blue";

            if (inferredRegion == "0")
            {
                correctness = "0";
            }
            else
                correctness = "1";

            //Target Region
            string targetRegion = "NA";
            if (currScene.region2Colour == "Green" && currScene.region1Colour != "Green")
            {
                targetRegion = currScene.regionID_1.ToString();
            }
            else if (currScene.region1Colour == "Green" && currScene.region2Colour != "Green")
            {
                targetRegion = currScene.regionID_2.ToString();
            }

            writer.WriteLine(nameOfUser + ", " + Placer3D.blockNum.ToString() + ", " + trialNum.ToString() + ", " + (currScene.isFeature ? "Feature" : "Error") + ", " + currScene.stimulusNumber.ToString() + ", " + currScene.numOfObjects.ToString() + ", "
                + (currScene.twoTargets ? "2" : "1") + ", " + currScene.regionID_1.ToString() + ", " + currScene.regionID_2.ToString() + ", " + currScene.region1Colour + ", " + currScene.region2Colour + ", "
                + (Time.time - timeAtStart).ToString() + ", " + "Blue" + ", " + inferredRegion + ", " + inferredTarget + ", " + targetRegion + ", " + correctness);

            Debug.Log(nameOfUser + ", " + Placer3D.blockNum.ToString() + ", " + trialNum.ToString() + ", " + (currScene.isFeature ? "Feature" : "Error") + ", " + currScene.stimulusNumber.ToString() + ", " + currScene.numOfObjects.ToString() + ", "
                + (currScene.twoTargets ? "2" : "1") + ", " + currScene.regionID_1.ToString() + ", " + currScene.regionID_2.ToString() + ", " + currScene.region1Colour + ", " + currScene.region2Colour + ", "
                + (Time.time - timeAtStart).ToString() + ", " + "Red" + ", " + inferredRegion + ", " + inferredTarget + ", " + targetRegion + ", " + correctness);

            for (int i = 0; i < toBeDisabled.Length; i++) //Set active
                toBeDisabled[i].SetActive(true);

            currScene = null; //We do not need this anymore
        }
    }

    void Trigger(object sender, VRTK.ControllerInteractionEventArgs e)
    {

        if (Placer3D.state == 2 ) //If all elements placed
        {
            poseWriterSwitch = false; //Stop tracking things

            Placer3D.state = -1; //Clean-up
            
            questionTarget = true; //Pose a question
        }
    }



    IEnumerator GetPoseEveryInstant()
    {

        while (poseWriterSwitch) //Is switched on and off at appropriate times
        {
            if (currScene != null)
            {
                PoseInfo pi = new PoseInfo(nameOfUser, currScene.stimulusNumber, trialNum, head.transform.position, torso.transform.position, head.transform.rotation, torso.transform.rotation);
                poseWriter.WriteLine(JsonUtility.ToJson(pi)); //Write whatever poses were captured
            }
            yield return new WaitForSeconds(0.02f);
        }
    }
}
