﻿//Logic for textual information such as post trial questions and post experiment goodbye.

using UnityEngine;
using System.Collections;

public class PerTrialTextChanges : MonoBehaviour {
    bool doOnce = false; 

	
	// Update is called once per frame
	void Update () {
        if(ControllerMaster.testOver && !doOnce)
        {
            for (int i = 0; i < ControllerMaster.toBeDisabled.Length; i++)
                ControllerMaster.toBeDisabled[i].SetActive(false);
            GetComponent<TextMesh>().text = "Thank you! Test complete. :)";
            Debug.Log("Thank you! Test complete. :)");
            ApplicationQuit(5.5f); // Quit after 5 and a half seconds
        }
        else
            GetComponent<TextMesh>().text = "";
        //Question to be asked about the trial
        if (ControllerMaster.questionTarget)
        {
            GetComponent<TextMesh>().text = "What colour was the target?\n[1] <color=#ff0000ff>Red</color>\n[2] <color=#0000ffff>Blue</color>\nUse the touchpad to answer.";
        }
        

    }
    IEnumerator ApplicationQuit(float time) //Quit after some time
    {
        yield return new WaitForSeconds(time);

        Application.Quit();
    }
}