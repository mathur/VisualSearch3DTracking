﻿//Last minute
//Definitely sub-optimal

using UnityEngine;
using System.Collections;

public class TextSwitch : MonoBehaviour {

    public GameObject FixationPlane; //It is momentarily disabled during the instruction play
    public GameObject ControllerImagePlane;
	// Update is called once per frame
	void Update () {
        switch (ControllerMaster.startScreenFrameNum)
        {
            case 1:
                //Re-enable Fixation scene
                FixationPlane.SetActive(true); //Deactivate the fixation plane and show the controller image
                ControllerImagePlane.SetActive(false);
                GetComponent<TextMesh>().text = "<b>Set-up</b> \nCan you find the two boxes on the floor?" +
                    "\nStep on the two boxes with one foot on each box. " +
                    "\nFace the two small squares in front of you and look directly at the fixation cross." +
                    "\nPress side buttons to continue.";
                break;
            case 2:
                GetComponent<TextMesh>().text = "<b>Description of experiment</b> \nYou will see multiple green cubes and spheres." +
                    "\nThere will be one odd element- either a <color=#ff0000ff>Red</color> cube or a <color=#0000ffff>Blue</color> cube." +
                    "\nYour task is to find the odd element as <b>quickly</b> as possible." +
                    "\nPress the trigger as <b>soon</b> as you see the odd element." +
                    "\nPress side buttons to continue.";
                break;
            case 3:
                GetComponent<TextMesh>().text = "<b>Description continued</b> \nYou can turn around in the 3D space to locate the target." +
                    "\nFor each trial, you will be asked to indicate the colour of the odd element,\nso please pay attention." +
                    "\nPress side buttons to continue.";
                break;
            case 4:
                GetComponent<TextMesh>().text = "<b>Description continued</b>  \nAfter the trial, you will be asked the color of the odd element" +
                    " Press [1] if the odd element was <color=#ff0000ff>Red</color> and [2] for <color=#0000ffff>Blue</color>." +
                    "\nA fixation screen will reappear to indicate the beginning of the next trial. " +
                    "\nIf you feel nauseous, take off the headset and inform the experimenter." +
                    "\nPress side buttons to continue.";
                break;
            case 0:
                FixationPlane.SetActive(false); //Deactivate the fixation plane and show the controller image
                ControllerImagePlane.SetActive(true);
                GetComponent<TextMesh>().text = "<b>Controller description</b> \nLook at the image in front of you." +
                    "\nIdentify the trigger, Button 1, Button 2 and the side buttons.\nYou shall need these during the experiment." +
                    "\nPress side buttons to continue.";
                break;
            case 5:
                //Recurring Fixation Scene text 
                GetComponent<TextMesh>().text = "Look directly at fixation cross while standing on the white platforms" +
                    "\nFind the odd element as <b>quickly</b> as possible.\nPull trigger as <b>soon</b> as you see it." +
                    "\nPress the side buttons to start.";
                break;
            case 666:
                GetComponent<TextMesh>().text = "<color=#00ff00ff>You can now take a 5 minute break\n. Please call the experimenter." +
                    "</color>\nPress the side buttons to continue.";
                break;
            default:
                //Error-> shouldn't be here
                Debug.LogError("Unknown value of ControllerMaster.startScreenFrameNum");
                break;
        }
    }
}
