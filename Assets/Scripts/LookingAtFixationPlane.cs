﻿//Attached to the movable head

using UnityEngine;
using System.Collections;

public class LookingAtFixationPlane : MonoBehaviour {
    public static bool valid = false;
    private int layerMaskForPlane;
	// Use this for initialization
	void Start () {
        layerMaskForPlane = LayerMask.GetMask("Fixation"); //Only hit the laer of Fixation GameObjects
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        Ray rayF = new Ray(transform.position, transform.forward); //forward
        RaycastHit hitF;

        Ray rayD = new Ray(transform.position, -transform.up); //downward
        RaycastHit hitD;

        if (Physics.Raycast(rayF, out hitF, 8, layerMaskForPlane) && Physics.Raycast(rayD, out hitD, 4, layerMaskForPlane)) //The plane can be max 8 / 4 metres away
        {
            valid = true;
        }
        else
            valid = false;

    }
}
